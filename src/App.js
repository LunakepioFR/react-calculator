import React, { useState } from 'react'; 
import './App.scss';



function App() {
  const [before, setBefore] = useState('');

  const [result, setResult] = useState('');
  function Button(props) {
    const handleClick = (e) => {
      if(e.target.name === '%' || e.target.name === '/' || e.target.name === '-' || e.target.name === '*' || e.target.name === '+'){
        if (e.target.name !== result.charAt(result.length-1)) {
          setResult(result.concat(e.target.name));
        }
      } else {
        setResult(result.concat(e.target.name))
      }
      if ( e.target.name === 'AC') {
        clear();
      } else if ( e.target.name === '=') {
        calculate();
        setBefore(result);
      }
    }

    const clear = () => {
      setResult('');
      setBefore('');
    }

    const calculate = () => {
      try {
        setResult(eval(result).toString());
      } catch (err) {
        setResult('Error');
      }
    }
    if ( props.number === 'x') {
      return <button name='*' onClick={handleClick}>{props.number}</button>
    }
    if ( props.number === ',') {
      return <button name='.' onClick={handleClick}>{props.number}</button>
    }
    return <button name={props.number} onClick={handleClick}>{props.number}</button>
  }
  
  return (
    <div className="App">
       <div className="calculator">
       <div className="display">
       <span class="before">{before}</span>
         <span>{result}</span>
       </div>
        <div className="row">
          <Button number="AC"/>
          <Button number="+-"/>
          <Button number="%"/>
          <Button number="/"/>
        </div>
        <div className="row">
          <Button number="7"/>
          <Button number="8"/>
          <Button number="9"/>
          <Button number="x"/>
        </div>
        <div className="row">
          <Button number="4"/>
          <Button number="5"/>
          <Button number="6"/>
          <Button number="-"/>
        </div>
        <div className="row">
          <Button number="1"/>
          <Button number="2"/>
          <Button number="3"/>
          <Button number="+"/>
        </div>
        <div className="row">
          <Button number="0"/>
          <Button number=","/>
          <Button number="="/>
        </div>
       </div>
    </div>
  );
}

export default App;
